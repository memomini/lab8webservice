package th.ac.tu.siit.lab8webservice;

import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String, String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Do not allow the user to change the orientation of the application
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String, String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, new String[] {
				"name", "value" }, new int[] { R.id.tvName, R.id.tvValue });
		setListAdapter(adapter);
	}

	@Override
	protected void onStart() {
		super.onStart();
		// check if the device has internet connection

		ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			// load data when there is a connection
			long current = System.currentTimeMillis();
			if (current - lastUpdate > 5 * 60 * 1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/bangkok.json");
			}
		} else {
			Toast t = Toast
					.makeText(
							this,
							"No Internet Connectivity on this device. Check your setting",
							Toast.LENGTH_LONG);
			t.show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch (id) {
		case R.id.refresh: {
			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// load data when there is a connection
				long current = System.currentTimeMillis();
				if (current - lastUpdate > 3*60 * 1000) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/bangkok.json");
					Toast t = Toast
							.makeText(this, "REfresh", Toast.LENGTH_LONG);
					t.show();
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
		}

			break;
		case R.id.bangkok: {

			ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// load data when there is a connection
				long current = System.currentTimeMillis();
				if (true) {
					WeatherTask task = new WeatherTask(this);
					task.execute("http://cholwich.org/bangkok.json");
					
				}
			} else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
		}

			break;

		case R.id.nonthaburi:{
			
			ConnectivityManager mgr = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				//load data when there is a connection
			long current = System.currentTimeMillis();
			if (true) {
			WeatherTask task = new WeatherTask(this);
			task.execute("http://cholwich.org/nonthaburi.json");
			
				}
			}
			else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
		}

			break;

		case R.id.pathumthani:{
			
			ConnectivityManager mgr = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = mgr.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				//load data when there is a connection
			long current = System.currentTimeMillis();
			if (true) {
			WeatherTask task = new WeatherTask(this);
			task.execute("http://cholwich.org/pathumthani.json");
			
				}
			}
			else {
				Toast t = Toast
						.makeText(
								this,
								"No Internet Connectivity on this device. Check your setting",
								Toast.LENGTH_LONG);
				t.show();
			}
		}
			break;

		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	class WeatherTask extends AsyncTask<String/* parameter */, Void, String> {
		Map<String, String> record;
		ProgressDialog dialog;

		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		// Executed in UI thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		// Executed in UI thread
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), result,
					Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			setTitle("Bangkok Weather");
		}

		// Executed in background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				// Get the parameter, set it as a URL
				URL url = new URL(params[0]);
				// Create a connection to the URL
				HttpURLConnection http = (HttpURLConnection) url
						.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				// We want to download data from the URL
				http.setDoInput(true);
				http.connect();

				response = http.getResponseCode();
				if (response == 200) { // OK for HTTP
					in = new BufferedReader(new InputStreamReader(
							http.getInputStream()));
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}

					JSONObject json = new JSONObject(buffer.toString());

					JSONObject jmain = json.getJSONObject("main");
					JSONObject jwind = json.getJSONObject("wind");

					list.clear();

					// Add "description"
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);

					String description = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);

					record = new HashMap<String, String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp") - 273.0;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp));
					list.add(record);

					String pressure = jmain.getString("pressure");
					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					record.put("value", pressure + "hPa");
					list.add(record);

					String humidity = jmain.getString("humidity");
					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					record.put("value", humidity + "%");
					list.add(record);

					/*
					 * String temp_min = jmain.getString("temp_min"); record =
					 * new HashMap<String,String>();
					 */

					double temp_min = jmain.getDouble("temp_min") - 273.0;
					record = new HashMap<String, String>();
					record.put("name", "Min Temperature");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp_min));
					list.add(record);

					double temp_max = jmain.getDouble("temp_max") - 273.0;
					record = new HashMap<String, String>();
					record.put("name", "Max Temperature");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp_max));
					list.add(record);

					/*
					 * JSONArray jwind = json.getJSONArray("wind"); JSONObject
					 * w1 = jwind.getJSONObject(0);
					 */

					String speed = jwind.getString("speed");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					record.put("value", speed + "mps");
					list.add(record);

					String deg = jwind.getString("deg");
					record = new HashMap<String, String>();
					record.put("name", "Wind Degree");
					record.put("value", deg + "degree");
					list.add(record);

					return "Finished Loading Weather Data";
				} else {
					return "Error " + response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}
		}
	}

}
